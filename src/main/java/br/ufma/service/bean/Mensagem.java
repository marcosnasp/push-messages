package br.ufma.service.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mensagem {

	@JsonProperty("tipo")
	private String tipo;
	
	@JsonProperty("apelido")
	private String apelido;
	
	@JsonProperty("texto")
	private String texto;

	public Mensagem() {
	}

	public Mensagem(String tipo, String apelido, String texto) {
		super();
		this.tipo = tipo;
		this.apelido = apelido;
		this.texto = texto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setNome(String tipo) {
		this.tipo = tipo;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
