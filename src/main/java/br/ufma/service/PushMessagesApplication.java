package br.ufma.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"br.ufma.service.services", "br.ufma.service.controller"})
public class PushMessagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PushMessagesApplication.class, args);
	}
}
