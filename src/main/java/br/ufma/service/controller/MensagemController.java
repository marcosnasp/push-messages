package br.ufma.service.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.ufma.service.bean.Mensagem;
import br.ufma.service.services.PushMessagesService;

@RestController
public class MensagemController {

	private static final Logger LOGGER = Logger.getLogger(MensagemController.class.getName());
	private static final String TOPIC_CURSO = "curso";

	@Autowired
	PushMessagesService pushMessagesService;

	@RequestMapping(value = "/send", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> send(@RequestBody Mensagem mensagem) throws JSONException {

		JSONObject body = new JSONObject();
		body.put("to", "/topics/" + TOPIC_CURSO);
		body.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", "Notificacao");
		notification.put("body", "Mensagem Enviada");

		JSONObject data = new JSONObject();
		data.put("tipo", mensagem.getTipo());
		data.put("apelido", mensagem.getApelido());
		data.put("texto", mensagem.getTexto());

		body.put("notification", notification);
		body.put("data", data);
		
		HttpEntity<String> request = new HttpEntity<>(body.toString());

		CompletableFuture<String> pushNotification = pushMessagesService.send(request);
		CompletableFuture.allOf(pushNotification).join();

		try {
			String firebaseResponse = pushNotification.get();
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			LOGGER.log(Level.WARNING, "Erro -> Interrupted Exception: ", e);
		} catch (ExecutionException e) {
			LOGGER.log(Level.WARNING, "Erro -> Execution Exception: ", e);
		}

		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}

}
